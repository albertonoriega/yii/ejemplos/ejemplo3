<?php

/** @var yii\web\View $this */

$this->title = 'Gestión de departamentos';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Gestión de departamentos</h1>

        <p class="lead">Gestión de empleados y departamentos de Alpe</p>

        
    </div>
    
    
    
</div>

<div class="body-content">

        <div class="row">
            <div>
            Creamos el CRUD de la base de datos de departamentos y empleados

            <ul>
                 <li>Importamos la base de datos a PHPmyadmin</li>
                 <li>Creamos los modelos y el CRUD para las dos tablas de la BBDD con gii</li>
                 <li>Cambiamos los label de todos los botones para ponerlos en castellano</li>
                    <ul>
                        <li>Vista index: Crear departamento</li>
                        <li>Vista view: Actualizar, Borrar, y cambiar el mensaje de confirmar borrado del registro</li>
                        <li>Vista update: Aceptar </li>
                    </ul>
                <li>Cambiamos el menu y el footer del layout</li>
            </ul>

        
            </div>

        </div>
    </div>
